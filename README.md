# Git repository for the paper New Algorithm for Exhausting Optimal Permutations for Generalized Feistel Networks

Julia 1.7 or higher is required to run the code as well as the packages

```
Pkg.add("Random")
Pkg.add("Combinatorics")
Pkg.add("SparseArrays")
```

Each file is independent and can be executed in any way you want.

Julia interactive mode is recommended.

Keep in mind that the parameters (k,R,...) are defined as const.

A multithread julia can be launched with

```
JULIA_NUM_THREADS=128 ./julia
```